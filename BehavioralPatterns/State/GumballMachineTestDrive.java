
public class GumballMachineTestDrive {

	public static void main(String[] args) {
	 
		GumballMachine gumballMachine = new GumballMachine(2);
        
        
        gumballMachine.dispense();
        gumballMachine.insertQuarter();
        gumballMachine.ejectQuarter();
        gumballMachine.insertQuarter();
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();
        gumballMachine.insertQuarter();
        gumballMachine.turnCrank();
        gumballMachine.ejectQuarter();
        gumballMachine.dispense();
        gumballMachine.insertQuarter();
        
     
	}
}
