
public class CarnesPizza extends Pizza {
	public CarnesPizza() {
		name = "Pizza de Carnes";
		dough = "Regular";
		sauce = "Tomate";
		toppings.add("Peperonni");
		toppings.add("Salame");
	}
}
