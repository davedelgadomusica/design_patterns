
public class RegularPizza extends Pizza {
	public RegularPizza() {
		name = "Pizza Regular";
		dough = "Masa Gruesa";
		sauce = "Tomate";
		toppings.add("Queso");
		toppings.add("Bocadillo");
		toppings.add("Arequipe");
	}
}
