
public class SimplePizzaFactory {

	public Pizza createPizza(String type) {
		Pizza pizza = null;

		if (type.equals("cheese")) {
			pizza = new CheesePizza();
		} else if (type.equals("pepperoni")) {
			pizza = new PepperoniPizza();
		} else if (type.equals("clam")) {
			pizza = new ClamPizza();
		} else if (type.equals("veggie")) {
			pizza = new VeggiePizza();
		}
		else if (type.equals("criolla")) {
			pizza = new CriollaPizza();
		}
	
		else if (type.equals("hawaian")) {
			pizza = new HawaianPizza();
		}
		else if (type.equals("mexican")) {
			pizza = new MexicanPizza();
		}
		else if (type.equals("carnes")) {
			pizza = new CarnesPizza();
		}
		else if (type.equals("regular")) {
			pizza = new RegularPizza();
		}
		return pizza;
	}
}
