public class PrototypePatternDemo {
   public static void main(String[] args) {
      ShapeCache.loadCache();

      Shape clonedShape = (Shape) ShapeCache.getShape("1");
      Shape clonedShape2 = (Shape) ShapeCache.getShape("1");
      
      System.out.println("Shape 1 : " + clonedShape.getType() +" Color: "+clonedShape.getColor());
      System.out.println("Shape 2 : " + clonedShape2.getType() + " Color: "+clonedShape2.getColor());

      clonedShape2.setColor("purple");
      System.out.println("Shape 1 : " + clonedShape);
      System.out.println("Shape 2 : " + clonedShape2);
      System.out.println("Shape 1 : " + clonedShape.getColor());
      System.out.println("Shape 2 : " + clonedShape2.getColor());
	
   }
}