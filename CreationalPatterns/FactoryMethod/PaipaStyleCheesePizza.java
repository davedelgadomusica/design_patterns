
public class PaipaStyleCheesePizza extends Pizza {

	public PaipaStyleCheesePizza() { 
		name = "Pizza de Queso de Paipa";
		dough = "Super Delgadita y Cocinada";
		sauce = "BBQ + Tomate";
 
		toppings.add("Queso Mozarella");
		toppings.add("Queso Campesino");
	}
 
	void box() {
	System.out.println("Se empaca con mucho amor en una bolsa tradicional Colombiana");
	}
}
