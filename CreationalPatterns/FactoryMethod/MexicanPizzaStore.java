
public class MexicanPizzaStore extends PizzaStore {

	Pizza createPizza(String item) {
        	if (item.equals("cheese")) {
            		return new MexicanCheesePizza();
        	} 
        	return null;
	}
}
