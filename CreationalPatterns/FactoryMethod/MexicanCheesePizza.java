
public class MexicanCheesePizza extends Pizza {

	public MexicanCheesePizza() { 
		name = "Pizza Mexicana con Mucho Queso";
		dough = "Borde de Bocadillo";
		sauce = "Normalita";
 
		toppings.add("Queso Rallao");
	}
}
