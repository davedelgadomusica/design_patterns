
public class MiniChips extends CondimentDecorator {
	Beverage beverage;

	public MiniChips(Beverage beverage) {
		this.beverage = beverage;
	}

	public String getDescription() {
		return beverage.getDescription() + ", MiniChips";
	}

	public double cost() {
		return .50 + beverage.cost();
	}
}
